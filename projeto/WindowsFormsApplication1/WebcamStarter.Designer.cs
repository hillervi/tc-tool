﻿namespace WindowsFormsApplication1
{
    partial class WebcamStarter
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBoxWebCam = new System.Windows.Forms.PictureBox();
            this.comboBoxWebCam = new System.Windows.Forms.ComboBox();
            this.buttonStart = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxWebCam)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBoxWebCam.Location = new System.Drawing.Point(12, 12);
            this.pictureBoxWebCam.Name = "pictureBox1";
            this.pictureBoxWebCam.Size = new System.Drawing.Size(700, 444);
            this.pictureBoxWebCam.TabIndex = 0;
            this.pictureBoxWebCam.TabStop = false;
            // 
            // comboBox1
            // 
            this.comboBoxWebCam.FormattingEnabled = true;
            this.comboBoxWebCam.Location = new System.Drawing.Point(133, 469);
            this.comboBoxWebCam.Name = "comboBox1";
            this.comboBoxWebCam.Size = new System.Drawing.Size(298, 21);
            this.comboBoxWebCam.TabIndex = 1;
            // 
            // button1
            // 
            this.buttonStart.Location = new System.Drawing.Point(455, 467);
            this.buttonStart.Name = "button1";
            this.buttonStart.Size = new System.Drawing.Size(75, 23);
            this.buttonStart.TabIndex = 2;
            this.buttonStart.Text = "Iniciar";
            this.buttonStart.UseVisualStyleBackColor = true;
            this.buttonStart.Click += new System.EventHandler(this.buttonStart_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 470);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(115, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Dispositivo de Câmera:";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(724, 500);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonStart);
            this.Controls.Add(this.comboBoxWebCam);
            this.Controls.Add(this.pictureBoxWebCam);
            this.Name = "Form1";
            this.Text = "Device";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FormWebCam_FormClosed);
            this.Load += new System.EventHandler(this.FormWebCam_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxWebCam)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBoxWebCam;
        private System.Windows.Forms.ComboBox comboBoxWebCam;
        private System.Windows.Forms.Button buttonStart;
        private System.Windows.Forms.Label label1;
    }
}

