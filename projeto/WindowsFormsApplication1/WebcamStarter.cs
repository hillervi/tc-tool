﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AForge.Video;
using AForge.Video.DirectShow;

namespace WindowsFormsApplication1
{

    public partial class WebcamStarter : Form
    {
        private FilterInfoCollection VideoCaptureDevices;
        private VideoCaptureDevice FinalVideoCaptureDevice;

        public WebcamStarter()
        {
           
            InitializeComponent();
        }

        

        private void FormWebCam_Load(object sender, EventArgs e)
        {
            VideoCaptureDevices = new FilterInfoCollection(FilterCategory.VideoInputDevice);
            foreach (FilterInfo VideoCaptureDevice in VideoCaptureDevices)
            {
                comboBoxWebCam.Items.Add(VideoCaptureDevice.Name);
            }

            comboBoxWebCam.SelectedIndex = 0;

            FinalVideoCaptureDevice = new VideoCaptureDevice(VideoCaptureDevices[comboBoxWebCam.SelectedIndex].MonikerString);
            FinalVideoCaptureDevice.NewFrame += FinalVideoCaptureDevice_NewFrame;
            FinalVideoCaptureDevice.Start();
        }

        private void buttonStart_Click(object sender, EventArgs e)
        {
            FinalVideoCaptureDevice.Stop();

            CoreGraphics CoreGraphics = new CoreGraphics();
            CoreGraphics.ShowDialog();
        }

        void FinalVideoCaptureDevice_NewFrame(object sender, NewFrameEventArgs eventArgs)
        {
            Bitmap image = (Bitmap)eventArgs.Frame.Clone();
            pictureBoxWebCam.Image = image;
        }

        private void FormWebCam_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (FinalVideoCaptureDevice.IsRunning)
            {
                FinalVideoCaptureDevice.Stop();
            }
        }


    }
}
